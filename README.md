
<img src="https://gitlab.com/uploads/-/system/project/avatar/45845598/package-svgrepo-com-4.png?width=96" width="60" />

# Laravel PackageMaker

## Installation

Install the package via composer:

```bash
$ composer require backtheweb/laravel-package-maker --dev
```
Publish the config file and edit it to your needs

```bash 
$ php artisan vendor:publish --provider="Backtheweb\PackageMaker\PackageMakerServiceProvider" --tag="config"
```

Create a new folder called `modules` (or wherever you want, don't forget update the config file) 
in the root of your Laravel project and add the following to your composer.json file:

```json
"repositories": [
  {
    "type": "path",
    "url": "modules/*/**",
    "options": {
    "symlink": false
  }
]
```

```bash
  $ php artisan make:package vendor/package
```

The command will create the following structure and files:

* packageName
    * config
        * packageName.php
    * src
        * PackageNameServiceProvider.php
    * tests
        * Feature
        * Unit
        * TestCase.php
    * .gitignore
    * CHANGELOG.md
    * composer.json
    * LICENSE
    * phpunit.xml
    * README.md

USer `--force` option to overwrite existing files

```bash
  $ php artisan make:package yourPackageName --force
```

## How Laravel package development

https://laravelpackage.com/#reasons-to-develop-a-package

## Credits

* logo: https://www.svgrepo.com/svg/347801/package
