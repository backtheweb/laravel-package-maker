<?php

return [
    'path' => 'modules',
    'username' => env('PACKAGE_MAKER_USERNAME'),
    'composer' => [
        'name' => '',
        'description' => '',
        'version' => 'dev-main',
        'type' => 'package',
        'homepage'  => '',
        'require' => [
            'php' => '^8.0',
        ],
        'require-dev' => [
            'orchestra/testbench' => '^8.0',
            'phpunit/phpunit' => '^10.1'
        ],
        'license' => [
            'MIT'
        ],
        'authors'   => [
            [
                'name'  => env('PACKAGE_MAKER_AUTHOR_NAME'),
                'email' => env('PACKAGE_MAKER_AUTHOR_EMAIL'),
            ]
        ],
        'autoload' => [
            'psr-4' => [], // required
        ],
        'autoload-dev' => [
            'psr-4' => [], // required
        ],
        'extra' => [
            'laravel' => [
                'providers' => [] // required
            ]
        ]
    ],
    'dirs' => [
        'config',
        // 'resources',
        // 'resources/views',
        // 'resources/views/components',
        'src/Console',
        // 'src/Models',
        // 'src/Http',
        // 'src/Http/Controllers',
        // 'src/View/',
        'tests/Feature',
        'tests/Unit',
    ],
    'stubs' => [

    ]
];
