<?php

namespace Backtheweb\PackageMaker;

use Illuminate\Support\Collection;

class ComposerFileMaker
{

    public array $settings = [];

    public function toArray() : array
    {
        return $this->settings;
    }

    public function __toString() : string
    {
        return json_encode($this->toArray(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    public static function factory(Array $settings = []) : ComposerFileMaker
    {
        $instance = new ComposerFileMaker();

        $instance->settings = $settings;

        return $instance;
    }

    public function save($pathName) : false|int
    {
        $content = $this->__toString();

        return file_put_contents($pathName, $content);
    }

    public function name(string $name) : ComposerFileMaker
    {
        $this->settings['name'] = strtolower($name);

        return $this;
    }

    public function description(string $description) : ComposerFileMaker
    {
        $this->settings['description'] = $description;

        return $this;
    }

    public function type(string $type) : ComposerFileMaker
    {
        $this->settings['type'] = $type;

        return $this;
    }

    public function homepage(string $homepage) : ComposerFileMaker
    {
        $this->settings['homepage'] = $homepage;

        return $this;
    }

    public function addAuthor(Array $author = []) : ComposerFileMaker
    {
        $this->settings['authors'][] = $author;

        return $this;
    }

    public function addProvider(string $provider)
    {
        $this->settings['extra']['laravel']['providers'][] = $provider;

        return $this;
    }

    public function addAutoloadPsr4(string $namespace, string $path)
    {
        $this->settings['autoload']['psr-4'][$namespace] = $path;

        return $this;
    }

    public function addAutoloadDevPsr4(string $namespace, string $path)
    {
        $this->settings['autoload-dev']['psr-4'][$namespace] = $path;

        return $this;
    }
}
