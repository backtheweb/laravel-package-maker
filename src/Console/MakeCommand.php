<?php

namespace Backtheweb\PackageMaker\Console;

use Backtheweb\PackageMaker\ComposerFileMaker;
use Illuminate\Console\Command;
//use Illuminate\Console\Concerns\CreatesMatchingTest;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class MakeCommand extends Command
{
    protected $signature = 'packager:make {name} {--force}';
    protected $description = '';
    private string $package;
    private string $packageName;
    private string $basePath;
    private string $namespace;
    private array $vars_keys = [];
    private array $vars_values = [];

    public static array $defaultComposer = [
        'name' => '',
        'description' => '',
        'version' => 'dev-main',
        'type' => 'package',
        'required' => [
            'php' => '^8.0',
        ],
        'require-dev' => [
            'orchestra/testbench' => '^8.0',
            'phpunit/phpunit' => '^10.1'
        ],
        'licenses' => [
            'MIT'
        ],
    ];

    public static array $defaultDirs = [
        'config',
        'src/Console',
        'tests/Feature',
        'tests/Unit',
    ];

    public static array $defaultStubs = [
        'CHANGELOG.md'                  => 'CHANGELOG.md',
        'LICENSE'                       => 'LICENSE',
        'phpunit.xml'                   => 'phpunit.xml',
        'README.md'                     => 'README.md',
        'config/config.php'             => 'config/{{ package }}.php',
        'src/ServiceProvider.php'       => 'src/{{ Package }}ServiceProvider.php',
        'src/Console/Command.php'       => 'src/Console/{{ Package }}Command.php',
        'tests/TestCase.php'            => 'tests/TestCase.php',
        'tests/Unit/PackageTest.php'    => 'tests/Unit/PackageTest.php',
        '.gitignore'                    => '.gitignore',
    ];

    /**
     * @param Filesystem $files
     */
    public function __construct(protected Filesystem $files)
    {
        parent::__construct();
    }

    public function handle() : int
    {
        $this->makeVars();
        $this->makeContainerPath();
        $this->makeDirs();
        $this->makeComposerJson();
        $this->makeFiles();;

        $this->info('Read the ' . $this->basePath . '/README.md file for next steeps.');

        return Command::SUCCESS;
    }

    public function makeVars() : void
    {
        $packageName       = $this->argument('name');
        $this->packageName = strtolower($packageName);

        $this->package     = $this->getPackage($this->packageName);
        $this->basePath    = $this->getBasePath($this->packageName);
        $this->namespace   = $this->getNamespace($this->packageName);

        $vars = [
            '{{ package }}'          => $this->package,
            '{{ Package }}'          => ucfirst($this->package),
            '{{ namespace }}'        => $this->namespace,
            '{{ packageName }}'      => strtolower($this->packageName),
            '{{ PackageName }}'      => ucfirst($this->packageName),
            '{{ ServiceProvider }}'  => ucfirst($this->package) . 'ServiceProvider',
            '{{ year }}'             => date('Y'),
            '{{ copyrightHolder }}'  => ucfirst(strtolower(config('package.username'))),
            '{{ appKey }}'           => config('app.key'),
        ];

        $this->vars_keys   = array_keys($vars);
        $this->vars_values = array_values($vars);
    }

    private function getNamespace(string $packageName) : string
    {
        try {

            list($vendor, $package) = explode('/', strtolower($this->packageName));
            $namespace = join( "\\", [ucfirst($vendor), ucfirst($package)]);

        } catch (\Throwable $th) {
            $this->info('Consider the package name in the format: vendor/package');
            $namespace = ucfirst($packageName);
        }

        return $namespace;
    }

    private function getPackage(string $packageName)
    {
        try {

            list($vendor, $package) = explode('/', strtolower($this->packageName));

        } catch (\Throwable $th) {

            $package = $packageName;
        }

        return strtolower($package);
    }

    private function getBasePath(string $packageName) : string
    {
        $packageName = strtolower($packageName);
        $packageName = str_replace('\\', '/', $packageName);
        return base_path(config('package.path') . '/' . $packageName);
    }

    private function makeContainerPath() : void
    {
        if ($this->files->exists($this->basePath) && !$this->option('force')) {
            $this->error('Package already exists!');
            return;
        }

        $this->files->makeDirectory($this->basePath, 0755, true, true);
    }

    private function makeComposerJson() : void
    {
        $package = Str::of($this->packageName)->replace('/', '-')->__toString();
        $json    = array_merge(self::$defaultComposer, config('package.composer'));

        ComposerFileMaker::factory($json)
            ->name( $this->packageName)
            ->addProvider($this->namespace . '\\' . ucfirst($this->package) . 'ServiceProvider')
            ->addAutoloadPsr4($this->namespace . '\\', 'src/')
            ->addAutoloadDevPsr4($this->namespace . '\\Tests\\', 'tests/')
            ->save($this->basePath . '/composer.json')
        ;
    }

    private function makeDirs() : void
    {
        $dirs = array_merge(self::$defaultDirs, config('package.dirs'));

        foreach ($dirs as $dir) {
            $this->files->makeDirectory($this->basePath . '/' . $dir, 0755, true, true);
        }
    }

    private function makeFiles() : void
    {
        $stubs = array_merge(self::$defaultStubs, config('package.stubs'));

        foreach($stubs as $stub => $target){

            if($target) {

                try {
                    $target   = str_replace($this->vars_keys, $this->vars_values, $target);
                    $fileName = $this->buildFile( $this->getStub($stub) , $target);
                } catch (\Exception $e) {
                    $this->error($e->getMessage());
                    continue;
                }


                $this->line('Created: ' . $fileName);
            }
        }
    }

    protected function buildFile(string $stub, string $target) : string
    {
        $fileName = $this->basePath . '/' . $target;
        $content  = str_replace($this->vars_keys, $this->vars_values, $stub);

        $this->files->put($fileName, $content);

        return $fileName;
    }

    protected function getStub($stub) : string {
        $path = realpath(__DIR__ . '/../../stubs/' . $stub . '.stub');
        return file_get_contents($path);
    }
}
