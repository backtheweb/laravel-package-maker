<?php

namespace Backtheweb\PackageMaker;

use Backtheweb\PackageMaker\Console\MakeCommand;
use Illuminate\Support\ServiceProvider;

class PackageMakerServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/package.php', 'package');
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                MakeCommand::class,
            ]);

            $this->publishes([
                __DIR__.'/../config/package.php' => config_path('package.php'),
            ], 'config');
        }
    }
}
